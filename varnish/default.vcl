#
# This is an example VCL file for Varnish.
#
# It does not do anything by default, delegating control to the
# builtin VCL. The builtin VCL is called when there is no explicit
# return statement.
#
# See the VCL chapters in the Users Guide at https://www.varnish-cache.org/docs/
# and https://www.varnish-cache.org/trac/wiki/VCLExamples for more examples.

# Marker to tell the VCL compiler that this VCL has been adapted to the
# new 4.0 format.
vcl 4.0;

import directors;

# Default backend definition. Set this to point to your content server.
backend default {
    .host = "SSL_TERMINATOR_REPLACE";
    .port = "8080";
}

backend inat {
#  .host = "static.inaturalist.org";
  .host = "13.33.18.227";
  .port = "80";
}

backend osm_a_tile {
#  .host = "a.tile.openstreetmap.org";
  .host = "130.239.18.114";
  .port = "80";
}

backend osm_b_tile {
#  .host = "b.tile.openstreetmap.org";
  .host = "130.236.254.221";
  .port = "80";
}

backend osm_c_tile {
#  .host = "c.tile.openstreetmap.org";
  .host = "130.239.18.114";
  .port = "80";
}

#backend mml_tile {
#  .host = "127.0.0.1";
#  .port = "8000";
#}

sub vcl_init {
  new osm = directors.round_robin();
  osm.add_backend(osm_a_tile);
  osm.add_backend(osm_b_tile);
  osm.add_backend(osm_c_tile);

}

sub vcl_recv {
  if (req.url ~ "^/server-status$" ||
      req.url ~ "^/chart2.*$" ||
      req.url ~ "^/img/.*$" ||
      req.url ~ "^/styles/.*$" ||
      req.url ~ "^/js.*$") {
          return (pass);
  }
  if (req.url ~ "^/osm/") {
    set req.url = regsub(req.url, "^/osm", "");
    set req.backend_hint = osm.backend();
#  } else if (req.url ~ "^/mml/") {
#    set req.http.host = "avoin-karttakuva.maanmittauslaitos.fi";
#    set req.url = regsub(req.url, "^/mml", "");
#    set req.backend_hint = mml_tile;
  } else if (req.url ~ "^/mml_wmts/") {
    set req.url = regsub(req.url, "^/mml_wmts", "/mml");
    set req.backend_hint = default;
  } else if (req.url ~ "^/inaturalist/") {
    set req.http.host = "static.inaturalist.org";
    set req.url = regsub(req.url, "^/inaturalist", "");
    set req.backend_hint = inat;
  } else if (req.url == "/varnish-status") {
    return(synth(200, "OK"));
  }

  unset req.http.cookie;
  unset req.http.Authorization;

  if (req.method != "GET" && req.method != "HEAD") {
    return (pass);
  }
}

sub vcl_backend_response {
  // Cache tiles for 1 year
  set beresp.ttl = 1y;
  set beresp.grace = 12w;

  // Remove all cookies
  unset beresp.http.set-cookie;
  unset beresp.http.cookie;

  if (beresp.status == 500 || beresp.status == 502 || beresp.status == 503 || beresp.status == 504 || beresp.status == 401)
  {
    return (abandon);
  }
}

sub vcl_deliver {
  if (obj.hits > 0) {
    set resp.http.X-Cache_v = "HIT";
  } else {
    set resp.http.X-Cache_v = "MISS";
  }
}
